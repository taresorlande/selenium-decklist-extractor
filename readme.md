# Selenium Decklist Extractor
O Selenium serefere a uma ferramenta de testes automatizados utilizada em aplicações web. A qual foi utilizada para navegar pelo site `mtgtop8.com`. 
Extraindo os dados referentes aos feitiços utilizados nos grimórios dos jogadores, os quais ficaram entre os 8 melhores nos torneios deste ano.


## Detalhamento da rotina
- Emparelhar o Google Chrome com Selenium através do `chromedriver.exe`.
- Conectar no site `mtgtop8.com`, e posicionar na pagina referente ao primeiro torneio do ano.
- Ir iterando página a pagina até percorrer todas as aproximadas `4000` paginas referentes aos torneios realizados este ano.
- Para cada torneio somente os dados do formato `Legacy` serão estraídos. 
- Capturar as informações dispostas na tela e gravar em um arquivo de texto.
- A rotina finaliza após percorrer todas as páginas.

#### Diagrama de Sequência
```mermaid
sequenceDiagram
    Python->>Selenium: Controla
    Selenium->>Chrome: Conecta (Chromedriver)
    Chrome->>MTGTop8: Acessa Página (mtgtop8.com)
    MTGTop8->>MTGTop8: Acessa Eventos (Event Id)
    MTGTop8->>Chrome: Dados do evento (Baralhos Vencedores)
    Chrome->>Selenium: Dados (Captura as Informações da Tela)
    Selenium->>Python: Dados (Array de Textos)
    Python->>Python: Tratamento de dados
    Python->>SO: Escreve o arquivo no Sistema Operacional (48 hrs)
    Python->>SO: Faz a litura dos arquivos dentro de cada subdiretório
    Python->>Python: Distribui os dados dos arquivos nas tabelas HASH (6 hrs)
    Python->>SO: Escreve os arquivos de resultado (.json)

```

#### Via Terminal
- Instalar dependências: `pip install -r requirements.txt`
- Executar a rotina: python3.8 `main.py`
