import tornado.web
import tornado.ioloop
import tornado.httpserver

import os
import json

class MainHandler(tornado.web.RequestHandler):
    def get(self, card):
        resultado = self.obter_similaridade(card)

        for res in resultado:
            self.write(res)
            self.write("<br>")
        # self.write("Running..")


    def obter_similaridade(self, card):
        arquivo = ""
        with open("similaridade.json") as f:
            arquivo = f.read()
        
        jArquivo = dict(reversed(sorted(json.loads(arquivo).items(), key=lambda item: item[1])))
        
        return self.obter_itens(jArquivo, card)


    def obter_itens(self, jArquivo, card):
        similares = []
        card = card+"@"        
        counter = 0

        for key in jArquivo:
            if card in key:
                similares.append(key.replace(card,""))
                counter += 1
            if counter >= 20:
                break
        
        return similares


def Application():
    application = tornado.web.Application(
        [
            (r"/card/?(?P<card>[^\/]+)?", MainHandler),
        ]
        # ,debug=True
    )
    application.listen(8080)


if __name__ == "__main__":

    Application()
    tornado.ioloop.IOLoop.current().start()
