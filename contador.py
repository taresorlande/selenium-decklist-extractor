import os


def walk_directory_files():
    list_number = 0
    mais_utilizadas = {}
    mais_utilizadas_side = {}
    afinidade_duplicada = {}

    source = os.path.dirname(os.path.realpath(__file__)) + "\\decklists"

    for root, dirs, files in os.walk(source):
        for file in files:
            filepath = os.path.join(root, file)

            decklist = extract_decklist(filepath)
            lista_mais_utilizadas = obter_lista_mais_utilizadas(decklist, mais_utilizadas)
            # lista_mais_utilizadas_side = obter_lista_mais_utilizadas_side(decklist, mais_utilizadas_side)
            # lista_afinidade_duplicada = obter_lista_afinidade_duplicada(decklist, afinidade_duplicada)

            list_number += 1

    print(lista_mais_utilizadas)
    # print(lista_mais_utilizadas_side)
    # print(lista_afinidade_duplicada)


def extract_decklist(filepath):
    with open(filepath, 'r') as f:
        read = f.read()

    decklist = read.split('\n')

    return decklist


def obter_lista_afinidade_duplicada(decklist, afinidade_duplicada):

    for item_1 in decklist:
        number, card_1 = obtain_card_value(item_1)
        number, card_1 = exclusor(number, card_1)

        for item_2 in decklist:

            number, card_2 = obtain_card_value(item_2)
            number, card_2 = exclusor(number, card_2)

            if card_1 == card_2:
                pass
            elif card_1 == "" or card_2 == "":
                pass
            else:
                key = f"{card_1}@{card_2}"
                try:
                    afinidade_duplicada[key] += 1
                except:
                    afinidade_duplicada[key] = 1

    return dict(reversed(sorted(afinidade_duplicada.items(), key=lambda item: item[1])))


def obter_lista_mais_utilizadas(decklist, mais_utilizadas):

    for item in decklist:
        number, card = obtain_card_value(item)
        number, card = exclusor(number, card)

        if card:
            try:
                mais_utilizadas[card] += int(number)
            except:
                mais_utilizadas[card] = int(number)

    return dict(reversed(sorted(mais_utilizadas.items(), key=lambda item: item[1])))


def obter_lista_mais_utilizadas_side(decklist, mais_utilizadas_side):
    ligado = False

    for item in decklist:
        number, card = obtain_card_value(item)

        if card == 'SIDEBOARD':
            ligado = True

        if card and ligado:

            try:
                int(number)
            except:
                continue

            number, card = exclusor(number, card)

            try:
                mais_utilizadas_side[card] += int(number)
            except:
                mais_utilizadas_side[card] = int(number)

    return dict(reversed(sorted(mais_utilizadas_side.items(), key=lambda item: item[1])))


def obtain_card_value(item):
    try:
        aux = item.split(' ')

        if len(aux) == 1:
            number = ''
            name = item
        else:
            number = aux.pop(0)
            name = ' '.join(aux)
    except:
        print("ERROR")

    return number, name


def exclusor(number, name):
    exclude_list = ['LANDS', 'CREATURES', 'INSTANTS and SORC.', 'OTHER SPELLS', 'SIDEBOARD']

    if name in exclude_list:
        number = ""
        name = ""

    if "LANDS" in name:
        number = ""
        name = ""

    return number, name


if __name__ == "__main__":

    walk_directory_files()


# def obtain_card_name(item):

#     aux = item.split(' ')

#     if len(aux) == 1:
#         return item
#     else:
#         aux.pop(0)
#         aux = ' '.join(aux)
#         return aux


# def copiar_arquivos_falhas(self, falhas):
#     this_path = os.path.dirname(os.path.realpath(__file__))

#     source = this_path + "/fontes/microsiga"
#     destination = this_path + "/saida/falhas/"

#     try:
#         os.mkdir(destination)
#     except:
#         os.system(f"rm -rf {destination}")
#         os.mkdir(destination)

#     for root, dirs, files in os.walk(source):
#         for file in files:
#             filename = file.upper()
#             if filename.endswith(".PRW") and filename in falhas:
#                 os.system(f"cp {os.path.join(root, file)} {destination}")
