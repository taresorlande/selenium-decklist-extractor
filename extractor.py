# from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium import webdriver

import time
import os


class Automaton:
    def __init__(self):
        chrome_service = Service('./chromedriver.exe')

        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("lang=pt-br")
        chrome_options.add_argument("--incognito")
        chrome_options.add_argument("--disable-gpu")
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--start-maximized")
        # chrome_options.add_argument("window-size=1000,900")

        self.driver = webdriver.Chrome(service=chrome_service, options=chrome_options)


    def clicar(self, path):
        try:
            elemento = self.element_presence(path)
            elemento.click()
        except:
            time.sleep(0.5)
            elemento = self.clicar(path)
        return elemento


    def element_presence(self, path):
        return WebDriverWait(self.driver, 5).until(EC.presence_of_element_located((By.XPATH, path)))


def get_event_decklists(bot: Automaton, event_number):
    bot.driver.get(f"https://www.mtgtop8.com/event?e={str(event_number)}")

    decklist_panel = "/html/body/div/div/div[7]/div[1]/div/div[1]"
    number_decklist = len(bot.element_presence(decklist_panel).find_elements(By.CLASS_NAME, "hover_tr")) + 1

    for row in range(number_decklist):
        imagem = decklist_panel + f"/div[{row+2}]/div/div[2]/a/img"
        
        try:
            time.sleep(1)
            bot.element_presence(imagem)
            bot.clicar(imagem)
            time.sleep(2)
        except:
            continue

        if "f=LE" in bot.driver.current_url:
            if row == 0:
                create_tournament_folder(event_number)

            decklist = extract_decklist(bot)
            deckname = (bot.element_presence(decklist_panel + f"/div[{row+2}]/div/div[3]/div[1]/a").text).replace("/","")
            print(f"#{row+1}_{deckname}")

            write_decklist(event_number, f"#{row+1}_{deckname}", decklist)
        else:
            break


def extract_decklist(bot: Automaton):
    grid = "/html/body/div/div/div[7]/div[2]/div[3]"
    number_panels = len(bot.element_presence(grid).find_elements(By.XPATH, "./*"))
    decklist = []

    for i in range(number_panels):
        panel = grid + f"/div[{i+1}]"
        extract_lines(bot, panel, decklist)

    return decklist


def extract_lines(bot: Automaton, panel, decklist):
    rows = len(bot.element_presence(panel).find_elements(By.TAG_NAME, "div"))

    for row in range(rows):
        try:
            texto = bot.element_presence( panel + f"/div[{row+1}]" ).text
            decklist.append(texto)
        except:
            pass


def write_decklist(event_number, deck_number, decklist):

    with open(f"decklists/{str(event_number)}/{deck_number}.txt", "w") as f:
        for line in decklist:
            f.write(f"{line}\n")


def create_tournament_folder(event_number):
    try:
        os.mkdir(f"decklists/{str(event_number)}")
    except:
        pass


if __name__ == "__main__":
    bot = Automaton()

    bot.driver.get("https://www.mtgtop8.com")
    bot.clicar("/html/body/div/div/div[6]/div[2]/button")

    event_number = 38120

    while event_number < 40120:
        print(event_number)
        get_event_decklists(bot, event_number)
        event_number += 1
